// Aqui temos a variável indicando quem está jogando, no caso quem inicia é o X.
var vez = "X";
// Aqui temos uma variável utilizada como contandor pro caso de todas as casas serem preenchidas e ninguem ganhar.
var contador = 0 ;
// Aqui temos um booleano indicando se o jogo ainda está acontecendo, com ela é possivel impedir os jogadores marcar nas casas após alguem ter vencido.
var rodando = true;
// Essa função serve para marcar o simbolo no quadrado
function selecionar(botao)
 {	
 	//para fazer isso primeiro é necessário saber se o quadrado não posssui conteudo e se jogo ainda está rodando
 	if(botao.innerHTML == "" && rodando == true){
 		//Aqui checa de quem é a vez e faz as alterações para passar a vez pro outro jogador
 		if (vez == "X") {
 			botao.innerHTML = "X";
 			document.getElementById("indicadorDaVez").innerHTML = "É a vez do O";
 			vez = "O";
 			contador++
 		}
 		//Aqui checa de quem é a vez e faz as alterações para passar a vez pro outro jogador
 		else if ( vez == "O") {
 			botao.innerHTML = "O";
 			document.getElementById("indicadorDaVez").innerHTML = "É a vez do X";
 			vez = "X";
 			contador++

 		}
 		// Esse if é pro caso comentado anteriormente, se o contador chegar a 9 e ninguem ganhar é porque deu velha.
 		if (contador == 9) {
 			document.getElementById("indicadorVencedor").innerHTML = "VELHA!";
 			document.getElementById("indicadorDaVez").innerHTML = "";
 		}
 		
 	}
 	// A cada jogada é chamada a função ganhou
	ganhou()	
}
// Zera todos as posições e recomeça o jogo
function resetar(){
	var i = 0;
	// Limpa o valor de todos os quadrados.
	while( i < 9 ){
        document.getElementsByClassName("casa")[i].innerHTML = "";
        i++;
    }
    //As condições iniciais são restabelecidas
    document.getElementById("indicadorDaVez").innerHTML = "É a vez do X";
    document.getElementById("indicadorVencedor").innerHTML = "";
    contador = 0;
    vez = "X";
    rodando = true;

}

// Verifica todos as combinações possíveis de se ganhar o jogo
function ganhou() {
	/*
		0    1    2 
		3	 4	  5                      
		6	 7	  8

	*/	

	// Em cada condicional ele checa se as 3 posições tem conteúdo igual, se isso for verdade é por que alguem ganhou
	// para mostrar quem ganhou ele pega a primeira posição dos 3.
	// o jogo acaba pois será atribuido false para a variavel rodando.

	// Situação de vitória 1: Diagonal Primária!!!!
	if(document.getElementsByClassName("casa")[0].innerHTML!= '' &&document.getElementsByClassName("casa")[0].innerHTML == document.getElementsByClassName("casa")[4].innerHTML
	 && document.getElementsByClassName("casa")[4].innerHTML == document.getElementsByClassName("casa")[8].innerHTML ) {
		document.getElementById("indicadorVencedor").innerHTML
		 = "O vencedor é : "+ document.getElementsByClassName("casa")[0].innerHTML;
		 rodando = false;}
	//Situação de vitória 2: Linha Superior!!!!
	else if(document.getElementsByClassName("casa")[0].innerHTML != '' && document.getElementsByClassName("casa")[0].innerHTML == document.getElementsByClassName("casa")[1].innerHTML
	 && document.getElementsByClassName("casa")[1].innerHTML == document.getElementsByClassName("casa")[2].innerHTML ) {
		document.getElementById("indicadorVencedor").innerHTML
		 = "O vencedor é : "+ document.getElementsByClassName("casa")[0].innerHTML;
		 rodando = false;}

	//Situação de vitória 3: Linha do Meio !!!!
	else if(document.getElementsByClassName("casa")[3].innerHTML != '' && document.getElementsByClassName("casa")[3].innerHTML == document.getElementsByClassName("casa")[4].innerHTML
	 && document.getElementsByClassName("casa")[4].innerHTML == document.getElementsByClassName("casa")[5].innerHTML ) {
		document.getElementById("indicadorVencedor").innerHTML
		 = "O vencedor é : "+ document.getElementsByClassName("casa")[3].innerHTML;
		 rodando = false;}
		
	//Situação de vitória 4: Linha inferior !!!!
	else if(document.getElementsByClassName("casa")[6].innerHTML != '' && document.getElementsByClassName("casa")[6].innerHTML == document.getElementsByClassName("casa")[7].innerHTML
	 && document.getElementsByClassName("casa")[7].innerHTML == document.getElementsByClassName("casa")[8].innerHTML ) {
		document.getElementById("indicadorVencedor").innerHTML
		 = "O vencedor é : "+ document.getElementsByClassName("casa")[6].innerHTML;
		 rodando = false;}
	//Situação de vitória 5: Coluna Esquerda !!!!
	else if(document.getElementsByClassName("casa")[0].innerHTML != '' && document.getElementsByClassName("casa")[0].innerHTML == document.getElementsByClassName("casa")[3].innerHTML
	 && document.getElementsByClassName("casa")[3].innerHTML == document.getElementsByClassName("casa")[6].innerHTML ) {
		document.getElementById("indicadorVencedor").innerHTML
		 = "O vencedor é : "+ document.getElementsByClassName("casa")[0].innerHTML;
		 rodando = false;}

	//Situação de vitória 6: Coluna do meio !!!!
	else if(document.getElementsByClassName("casa")[1].innerHTML != '' && document.getElementsByClassName("casa")[1].innerHTML == document.getElementsByClassName("casa")[4].innerHTML
	 && document.getElementsByClassName("casa")[4].innerHTML == document.getElementsByClassName("casa")[7].innerHTML ) {
		document.getElementById("indicadorVencedor").innerHTML
		 = "O vencedor é : "+ document.getElementsByClassName("casa")[1].innerHTML;
		 rodando = false;}

	// Situação de vitória 7: Coluna da Direita!!!!
	else if(document.getElementsByClassName("casa")[2].innerHTML != '' && document.getElementsByClassName("casa")[2].innerHTML == document.getElementsByClassName("casa")[5].innerHTML
	 && document.getElementsByClassName("casa")[5].innerHTML == document.getElementsByClassName("casa")[8].innerHTML ) {
		document.getElementById("indicadorVencedor").innerHTML
		 = "O vencedor é : "+ document.getElementsByClassName("casa")[2].innerHTML;
		 rodando = false;}
  	//Situação de vitória 8:Diagonal Secundária!!!!
  	else if(document.getElementsByClassName("casa")[2].innerHTML != '' && document.getElementsByClassName("casa")[2].innerHTML == document.getElementsByClassName("casa")[4].innerHTML
	 && document.getElementsByClassName("casa")[4].innerHTML == document.getElementsByClassName("casa")[6].innerHTML ) {
		document.getElementById("indicadorVencedor").innerHTML
		 = "O vencedor é : "+ document.getElementsByClassName("casa")[2].innerHTML;
		 rodando = false;}

}